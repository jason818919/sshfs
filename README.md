# **Introduction**
------
With ssh file system client, you can mount the remote file system to your own computer.
Access your remote file by streaming to local. Of course, you need a remote sftp server.

Note that sshfs can be used on Linux, Mac OS X and Windows.

# **Usage**
------
## Linux
```
$ apt-get install sshfs
$ mkdir ~/remote_disk
```
Mount the remote file system.
```
$ sshfs <user>@<host>:/path/to/remote/folder ~/remote_disk -o port=<port>
```
If you have permission problem, run the following command instead.
```
$ sshfs <user>@<host>:/path/to/remote/folder ~/remote_disk -o port=<port> -o defer_permissions
```
When you want to disconnect this remote, unmount it as following.
```
$ fusermount -u ~/remote_disk
```
## Mac OS X
First, install brew.
```
$ usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
Then, install sshfs.
```
$ brew cask install sshfs
```
Mount the remote file system.
```
$ sshfs <user>@<host>:/path/to/remote/folder ~/remote_disk -o port=<port>
```
Add the defer_permissions option if you have permission problem.
## Windows
Install [win-sshfs](https://code.google.com/archive/p/win-sshfs/).
Please see the source or download the executable by the following page.

#**Author**

Jason Chen
